package com.depenbi.server.service;

import com.depenbi.server.domain.ModelConf;

import java.util.List;

public interface ModelConfService {

    public void save(ModelConf modelConf);

    public void update(ModelConf modelConf);

    public void delete(ModelConf modelConf);

    public ModelConf getModelConf(String NIP);

    public List<ModelConf> getModelConfs();
}
