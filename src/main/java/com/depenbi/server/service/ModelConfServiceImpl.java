package com.depenbi.server.service;

import com.depenbi.server.domain.ModelConf;
import com.depenbi.server.repository.ModelConfRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class ModelConfServiceImpl implements ModelConfService {

    @Autowired
    private ModelConfRepository modelConfRepository;

    @Transactional
    @Override
    public void save(ModelConf modelConf) {
        modelConfRepository.save(modelConf);
    }

    @Transactional
    @Override
    public void update(ModelConf modelConf) {
        modelConfRepository.update(modelConf);
    }

    @Transactional
    @Override
    public void delete(ModelConf modelConf) {
        modelConfRepository.delete(modelConf);
    }

    @Override
    public ModelConf getModelConf(String NIP) {
        return modelConfRepository.getModelConf(NIP);
    }

    @Override
    public List<ModelConf> getModelConfs() {
        return modelConfRepository.getModelConfs();
    }

}