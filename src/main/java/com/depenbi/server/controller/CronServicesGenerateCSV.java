package com.depenbi.server.controller;

import com.depenbi.common.util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Deni Utama on 14/08/2021.
 */

@Configuration
@EnableScheduling
//Main class
public class CronServicesGenerateCSV {

    //ModelSessionConf conf = modelSessionServiceConf.getModelSessionConf(1);
    @Scheduled(fixedRate = 50000)
    public void scheduleFixedRateTask() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, -9420);
        date = calendar.getTime();
        String dob = dateFormat.format(date);
        String NIP = dob.replace("-","")+util.getNIP();
        String path = "new/"+NIP+".csv";

        try (PrintWriter writer = new PrintWriter(new File(path))) {
            StringBuilder sb = new StringBuilder();
            sb.append("NIP");
            sb.append(',');
            sb.append("NAME");
            sb.append(',');
            sb.append("DATE_OF_BIRTH");
            sb.append(',');
            sb.append("GENDER");
            sb.append(',');
            sb.append("ADDRESS");
            sb.append(',');
            sb.append("ID_NUMBER");
            sb.append(',');
            sb.append("PHONE_NUMBER");
            sb.append('\n');

            sb.append(NIP);
            sb.append(',');
            sb.append(util.getAfabet());
            sb.append(',');
            sb.append(dob);
            sb.append(',');
            sb.append(util.getGender());
            sb.append(',');
            sb.append(util.getAfabet());
            sb.append(',');
            sb.append(util.getAfaNumberik());
            sb.append(',');
            sb.append(util.getPhoneNumber());
            sb.append('\n');
            writer.write(sb.toString());
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }
}