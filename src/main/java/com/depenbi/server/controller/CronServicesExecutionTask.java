package com.depenbi.server.controller;


import com.depenbi.common.*;
import com.depenbi.server.domain.ModelConf;
import com.depenbi.server.service.ModelConfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Deni Utama on 14/08/2021.
 */

@Configuration
@EnableScheduling
//Main class
public class CronServicesExecutionTask {

    @Autowired
    private ModelConfService modelConfService;


    //ModelSessionConf conf = modelSessionServiceConf.getModelSessionConf(1);
    @Scheduled(fixedRate = 8000000)
    public void scheduleFixedRateTask() {
        String line = "";
        String fileExecution = "";
        String fileExecutionTemp = "";
        String splitBy = ",";
        String NIP = "";
        String name = "";
        String dob = "";
        String gender = "";
        String address = "";
        String idNumber = "";
        String phoneNumber = "";
        Date dobDate = null;

        try {
            Path currentRelativePath = Paths.get("");
            String curPath = currentRelativePath.toAbsolutePath().normalize().toString();
            File[] files = new File(curPath+"/new/").listFiles(obj -> obj.isFile() && obj.getName().endsWith(".csv"));
            fileExecution = files[0].getName();
            Path temp = Files.move(Paths.get(curPath+"/new/"+fileExecution), Paths.get(curPath+"/done/"+fileExecution));

            BufferedReader br = new BufferedReader(new FileReader(curPath+"/done/"+fileExecution));
            while ((line = br.readLine()) != null){
                String[] data = line.split(splitBy);    // use comma as separator
                if (!data[0].equals("NIP")){
                    NIP = data[0];
                    name = data[1];
                    dob = data[2];
                    gender = data[3];
                    address = data[4];
                    idNumber = data[5];
                    phoneNumber = data[6];
                    String validateSIM = validation.validasiSIM(name,dob,gender,address,idNumber,phoneNumber);
                    if (validateSIM.equals("true")){
                       if(modelConfService.getModelConf(NIP).getNIP().length() >= 1){
                           System.out.println("NIP Can't be duplicated");
                           Files.move(Paths.get(curPath+"/done/"+fileExecution), Paths.get(curPath+"/reject/"+fileExecution));
                       }else {
                           DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                           dobDate = dateFormat.parse(dob);

                           ModelConf dataSave = new ModelConf();
                           dataSave.setNIP(NIP);
                           dataSave.setName(name);
                           dataSave.setDob(dobDate);
                           dataSave.setGender(gender);
                           dataSave.setAddress(address);
                           dataSave.setIdNumber(idNumber);
                           dataSave.setPhoneNumber(phoneNumber);
                           modelConfService.save(dataSave);
                       }
                    }else{
                        System.out.println(validateSIM);
                        Files.move(Paths.get(curPath+"/done/"+fileExecution), Paths.get(curPath+"/reject/"+fileExecution));
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}