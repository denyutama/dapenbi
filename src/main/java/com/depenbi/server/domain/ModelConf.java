package com.depenbi.server.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "T_SA_Registration")
public class ModelConf implements Serializable {

    @Id
    @Column(name = "NIP", length = 18)
    private String NIP;

    @Column(name = "name" , length = 25)
    private String name;

    @Column(name = "dob", length = 8)
    private Date dob;

    @Column(name = "gender", length = 2)
    private String gender;

    @Column(name = "address" , length = 35)
    private String address;

    @Column(name = "idNumber" , length = 20)
    private String idNumber;

    @Column(name = "phoneNumber" , length = 15)
    private String phoneNumber;

    public String getNIP() {
        return NIP;
    }

    public void setNIP(String NIP) {
        this.NIP = NIP;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

}