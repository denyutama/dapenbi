package com.depenbi.server.repository;

import com.depenbi.server.domain.ModelConf;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ModelConfRepositoryImpl implements ModelConfRepository {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void save(ModelConf modelConf) {
        sessionFactory.getCurrentSession().save(modelConf);
    }

    @Override
    public void update(ModelConf modelConf) {
        sessionFactory.getCurrentSession().update(modelConf);
    }

    @Override
    public void delete(ModelConf modelConf) {
        sessionFactory.getCurrentSession().delete(modelConf);
    }

    @Override
    public ModelConf getModelConf(String NIP) {
        return sessionFactory.getCurrentSession().get(ModelConf.class, NIP);
    }

    @Override
    public List<ModelConf> getModelConfs() {
        return sessionFactory.getCurrentSession().createCriteria(ModelConf.class).list();
    }

}
