package com.depenbi.server.repository;

import com.depenbi.server.domain.ModelConf;

import java.util.List;

public interface ModelConfRepository {

    public void save(ModelConf modelCOnf);

    public void update(ModelConf modelConf);

    public void delete(ModelConf modelConf);

    public ModelConf getModelConf(String NIP);

    public List<ModelConf> getModelConfs();
}
