package com.depenbi.test;

import com.depenbi.common.validation;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.*;

public class testExecutionTask {
    public static void main(String[] args) {
        String line = "";
        String fileExecution = "";
        String fileExecutionTemp = "";
        String splitBy = ",";
        String NIP = "";
        String name = "";
        String dob = "";
        String gender = "";
        String address = "";
        String idNumber = "";
        String phoneNumber = "";
        try {
        Path currentRelativePath = Paths.get("");
        String curPath = currentRelativePath.toAbsolutePath().normalize().toString();
        File [] files = new File(curPath+"/new/").listFiles(obj -> obj.isFile() && obj.getName().endsWith(".csv"));
        fileExecution = files[0].getName();
        Path temp = Files.move(Paths.get(curPath+"/new/"+fileExecution), Paths.get(curPath+"/done/"+fileExecution));

            BufferedReader br = new BufferedReader(new FileReader(curPath+"/done/"+fileExecution));
            while ((line = br.readLine()) != null){
                String[] data = line.split(splitBy);    // use comma as separator
                if (!data[0].equals("NIP")){
                    NIP = data[0];
                    name = data[1];
                    dob = data[2];
                    gender = data[3];
                    address = data[4];
                    idNumber = data[5];
                    phoneNumber = data[6];
                    String validateSIM = validation.validasiSIM(name,dob,gender,address,idNumber,phoneNumber);
                    if (validateSIM.equals("true")){
                        System.out.println("validate is good");
                    }else{
                        System.out.println(validateSIM);
                        Files.move(Paths.get(curPath+"/done/"+fileExecution), Paths.get(curPath+"/reject/"+fileExecution));
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
