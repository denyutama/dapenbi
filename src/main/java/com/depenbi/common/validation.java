package com.depenbi.common;

import java.text.DateFormat;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class validation {

    public static String validasiSIM (String name, String  dob, String  gender, String  address, String  idNumber, String  phoneNumber) {

        String result = "";
        Date dobDate = null;
        Date sysDate = null;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date sysDateTemp = new Date();
        try {
            sysDate = dateFormat.parse(dateFormat.format(sysDateTemp));
            dobDate = dateFormat.parse(dob);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (!(name.matches("[a-zA-Z ]*"))) {
            result = "Name shouldn't have any special character";
        }
        else if (dobDate.equals(sysDate) || dobDate.after(sysDate)){
            result = "date of birth shouldn't be today or future";
        }
        else if (!(gender.equals("F") || gender.equals("M"))){
            result = "gander can only F or M";
        }
        else if (address.length() <= 20){
            result = "address must at least be 20 character long";
        }
        else if (!(idNumber.matches("^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$"))){
            result = "should be mix character and number";
        }
        else if (!(phoneNumber.matches("^\\+(?:[0-9] ?){6,14}[0-9]$"))){
            result = "should comply to country's standard";
        }else{
            result = "true";
        }

        return result;
    }

}
